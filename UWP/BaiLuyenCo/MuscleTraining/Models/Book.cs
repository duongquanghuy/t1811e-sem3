﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuscleTraining.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }

    }
    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var Books = new List<Book>();
            Books.Add(new Book { BookId = 1, Title = "vulpate", Author = "auturn", CoverImage = "Assets/1.png" });
            Books.Add(new Book { BookId = 2, Title = "culpate", Author = "buturn", CoverImage = "Assets/2.png" });
            Books.Add(new Book { BookId = 3, Title = "aulpate", Author = "cuturn", CoverImage = "Assets/3.png" });
            Books.Add(new Book { BookId = 4, Title = "eulpate", Author = "duturn", CoverImage = "Assets/4.png" });
            Books.Add(new Book { BookId = 5, Title = "rulpate", Author = "xuturn", CoverImage = "Assets/5.png" });
            Books.Add(new Book { BookId = 6, Title = "tulpate", Author = "yuturn", CoverImage = "Assets/6.png" });
            Books.Add(new Book { BookId = 7, Title = "uulpate", Author = "zuturn", CoverImage = "Assets/7.png" });
            Books.Add(new Book { BookId = 8, Title = "qulpate", Author = "iuturn", CoverImage = "Assets/8.png" });
            Books.Add(new Book { BookId = 9, Title = "eulpate", Author = "kuturn", CoverImage = "Assets/9.png" });
            Books.Add(new Book { BookId = 10, Title = "aulpate", Author = "luturn", CoverImage = "Assets/10.png" });
            Books.Add(new Book { BookId = 11, Title = "lulpate", Author = "nuturn", CoverImage = "Assets/11.png" });
            Books.Add(new Book { BookId = 12, Title = "kulpate", Author = "euturn", CoverImage = "Assets/12.png" });
            Books.Add(new Book { BookId = 13, Title = "hulpate", Author = "futurn", CoverImage = "Assets/13.png" });

            return Books;

        }
    }
}
