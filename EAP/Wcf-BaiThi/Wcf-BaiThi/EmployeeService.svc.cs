﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Wcf_BaiThi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeService.svc or EmployeeService.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeService : IEmployeeService
    {
        DataEmployeeDataContext data = new DataEmployeeDataContext();
        public bool AddEmployee(Employee employee)
        {
            try
            {
                data.Employees.InsertOnSubmit(employee);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Employee> SearchEmployee(string dep)
        {
            try
            {
                return (from e in data.Employees where( e.Department.EndsWith(dep)) select e).ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<Employee> GetEmployees()
        {
            try
            {

                return (from emp in data.Employees select emp).ToList();
            }
            catch
            {
                return null;
            }
        }
    }
}
