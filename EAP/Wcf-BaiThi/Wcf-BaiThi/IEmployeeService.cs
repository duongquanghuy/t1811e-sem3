﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Wcf_BaiThi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmployeeService" in both code and config file together.
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json,
              ResponseFormat = WebMessageFormat.Json,
              UriTemplate = "v1/GetEmployeeList",
              BodyStyle = WebMessageBodyStyle.Bare)]
        List<Employee> GetEmployees();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "v1/AddEmployee",
           BodyStyle = WebMessageBodyStyle.Bare)]
        bool AddEmployee(Employee employee);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "v1/SearchEmployee",
        BodyStyle = WebMessageBodyStyle.Bare)]
        List<Employee> SearchEmployee(string dep);
    }
}
