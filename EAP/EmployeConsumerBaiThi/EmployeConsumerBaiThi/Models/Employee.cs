﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeConsumerBaiThi.Models
{
    public class Employee
    {
        public int ID { get; set; }
        public string EmpName { get; set; }
        public int? Salary { get; set; }

        public string Department { get; set; }

        public string Address { get; set; }
    }
}