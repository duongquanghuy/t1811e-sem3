﻿using EmployeConsumerBaiThi.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeConsumerBaiThi.ServiceReference1;
using WebGrease.Css.Extensions;

namespace EmployeConsumerBaiThi.Models
{
    public class EmployeeSerclient
    {
        EmployeeServiceClient client = new EmployeeServiceClient();
        public List<Employee> GetAllEmployee()
        {
            var list = client.GetEmployees();

            var rt = new List<Employee>();
            list.ForEach(b => rt.Add(new Employee() { ID = b.ID , Department = b.Department, Address = b.Address, EmpName = b.EmpName, Salary = b.Salary }));

            return rt;
        }
        public bool AddEmployee(Employee employee)
        {
            if (employee.EmpName == "")
            {
                return false;
            }
            var newEmployee = new ServiceReference1.Employee()
            {
                ID = employee.ID,
                Address = employee.Address,
                EmpName = employee.EmpName,
                Department = employee.Department,
                Salary = employee.Salary
            };
            client.AddEmployee(newEmployee);
            return true;
        }
        public List<Employee> SearchEmployee(string dep)
        {
            var list = client.SearchEmployee(dep);

            var rt = new List<Employee>();
            list.ForEach(b => rt.Add(new Employee() { ID = b.ID, Department = b.Department, Address = b.Address, EmpName = b.EmpName, Salary = b.Salary }));

            return rt;
        }
    }
}