﻿using EmployeConsumerBaiThi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace EmployeConsumerBaiThi.Controllers
{
  
    public class EmployeeController : Controller
    {
        EmployeeSerclient emp = new EmployeeSerclient();
        // GET: Employee
        public ActionResult Index()
        {
            var model = emp.GetAllEmployee();
            return View(model.ToList());
           
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            try
            {

                bool rt = emp.AddEmployee(employee);
                if (rt == true)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Search(string SearchString)
        {
            var model = emp.SearchEmployee(SearchString);
            if(model == null)
            {
                return RedirectToAction("Index" , "Employee");
            }
            return View(model);
        }
    }
}
