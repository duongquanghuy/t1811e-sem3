package com.example.fullstackrestspringboottodoapplication.controller;


import com.example.fullstackrestspringboottodoapplication.entity.Todo;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class TodoController {
    @GetMapping("/")
    public String list(){
        return "index2";
    }
}
