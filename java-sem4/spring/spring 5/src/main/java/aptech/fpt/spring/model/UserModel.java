package aptech.fpt.spring.model;

import aptech.fpt.spring.entity.Product;
import aptech.fpt.spring.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;


public interface UserModel extends CrudRepository<User, Integer> {
    Page<User> findAll(Pageable pageable);
}
