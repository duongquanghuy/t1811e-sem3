package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.Product;
import aptech.fpt.spring.entity.User;
import aptech.fpt.spring.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.List;

@Controller
public class AuthController {
    @Autowired
    UserModel userModel;

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(path = "/user/list" , method = RequestMethod.GET)
    public String getListProduct(Model model, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = (Page<User>) userModel.findAll(PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        model.addAttribute("datetime", Calendar.getInstance().getTime());
        return "userManager";
    }

    @RequestMapping(path = "/user/create", method = RequestMethod.GET)
    public String register(@ModelAttribute  User user) {
        return "register";
    }
    @RequestMapping(path = "/user/create", method = RequestMethod.POST)
    public String createUser(@ModelAttribute User user, Model model ) {
        model.addAttribute("user" ,user);
        userModel.save(user);
        return "redirect:/user/list";
    }
}

