package aptech.fpt.spring.controller;

import aptech.fpt.spring.entity.Product;
import aptech.fpt.spring.entity.User;
import aptech.fpt.spring.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

@Controller
public class UserController {
    @Autowired
    UserModel userModel;

    @RequestMapping(path = "/user/edit/{id}" , method = RequestMethod.GET)
    public String editProduct(@PathVariable int id, Model model) {
        Optional<User> user = userModel.findById(id);
        if (user.isPresent()) {
            model.addAttribute("user", user.get());
            return "register";
        } else {
            return "not-found";
        }
    }
    @RequestMapping(path = "/user/delete/{id}" , method = RequestMethod.GET)
    public String editUser(@PathVariable int id, Model model) {
        Optional<User> optionalUser = userModel.findById(id);
        if (optionalUser.isPresent()) {
            userModel.delete(optionalUser.get());
            return "redirect:/user/list";
        } else {
            return "not-found";
        }
    }

}
