package aptech.fpt.spring.entity;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank(message = "Tên không thể để trống.")
    @Size(min = 8, message = "Tên phải dài hơn 8 ký tự.")
    private String name;
    private String address;
    @NotBlank(message = "Username ko duoc de trong.")
    @Size(min = 6 , message = "Username toi thieu 6 ky tu")
    private String username;
    @NotBlank(message = "Password ko duoc de trong.")
    @Size(min = 6 , message = "password toi thieu 6 ky tu")
    private String password;

    public User() {
    }

    public User( @NotNull @Size(min = 8) String name, String address, @NotNull @Size(min = 6) String username, @NotNull @Size(min = 6) String password) {
        this.name = name;
        this.address = address;
        this.username = username;
        this.password = password;
    }
    public User(int id, @NotNull @Size(min = 8) String name, String address, @NotNull @Size(min = 6) String username, @NotNull @Size(min = 6) String password) {
        this.id=  id;
        this.name = name;
        this.address = address;
        this.username = username;
        this.password = password;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
