package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    // DI call model step 2
    @Autowired
    UserModel userModel;


    @RequestMapping("/")
    public  String index(Model model){

        List<User> users = (List<User>) userModel.findAll();
        //request.addAttribute
        model.addAttribute("users" , users);
        return  "index";
    }
    @RequestMapping(value = "add")
    public String addUser(Model model){
        model.addAttribute("user" , new User());
        return "addUser";
    }

    @RequestMapping(value = "/save" , method = RequestMethod.POST)
    public String save(User user){
        userModel.save(user);
        return "redirect:/";
    }
    @RequestMapping(value = "/edit" , method = RequestMethod.GET)
    public String editUser(@RequestParam("id") int userId , Model model){
        Optional<User> userEdit = userModel.findById(userId);
        userEdit.ifPresent(user -> model.addAttribute("user" , user));
        return "editUser";

    }
    @RequestMapping(value = "/delete/{id}" , method = {RequestMethod.GET})
    public String delete(@PathVariable int id) {
        Optional<User> optionalProduct = userModel.findById(id);
        return "redirect:/";
    }

}
