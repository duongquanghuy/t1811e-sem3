package com.example.demo.model;

import com.example.demo.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserModel extends CrudRepository< User ,Integer > {

}
