﻿using BaiThiASP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace BaiThiASP.DAl
{
    public class StoreContext:DbContext
    {
        public StoreContext() : base("StoreContext") { }

        public DbSet<Category> Customers { get; set; }
        public DbSet<Product> categories { get; set; }
   

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

        }
    }
}