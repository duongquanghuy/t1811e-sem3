﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaiThiASP.Models
{
    public class Product
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Product Name")]
        [Required(ErrorMessage = "Product Name is requied")]
        [StringLength(150)]
        public string ProductName { get; set; }

        [DisplayName("Product ID")]
        [Required(ErrorMessage = "Product ID is requied")]
        public int SupplierID { get; set; }

        [DisplayName("Quantity PerUnit ")]
        [Required(ErrorMessage = "Quantity PerUnit  is requied")]
        public long QuantityPerUnit { get; set; }

        [DisplayName("Unit Price")]
        [Range(3000, 1000000, ErrorMessage = "salary ,must between 3000 anh 1000000")]
        public int UnitPrice { get; set; }

        [DisplayName("Unitsln Stock")]
        public int UnitslnStock { get; set; }

        [DisplayName("Unitsln On Order")]
        public int UnitslnOnOrder { get; set; }

        [DisplayName("Reorder Lavel")]
        public int ReorderLavel { get; set; }

        [DisplayName("Disconrinued")]
        public int Disconrinued { get; set; }

        [DisplayName("Category")]
        public int? CategoryID { get; set; }

        public virtual Category Category { get; set; }



    }
}