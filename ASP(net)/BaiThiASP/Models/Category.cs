﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaiThiASP.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Category Name")]
        [StringLength(150 , MinimumLength =4)]
        public string CategoryName { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Description")]
        public string Picture { get; set; }

        public virtual ICollection<Product>  Products { get; set; }
    }
}