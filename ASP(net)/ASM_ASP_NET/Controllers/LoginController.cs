﻿using ASM_ASP_NET.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using ASM_ASP_NET.Models;
using System.Text;
using Microsoft.Ajax.Utilities;

namespace ASM_ASP_NET.Controllers
{
    public class LoginController : Controller
    {
        Restaurant db = new Restaurant();
        
        // GET: Login
        public ActionResult Index()
        {

            if (Session["ID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        //get register
        public ActionResult Register()
        {
            return View();
        }



        // POST: Login/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Employee employee)
        {
            if (ModelState.IsValid)
            {
               
                var check = db.Employees.FirstOrDefault(e => e.UserName == employee.UserName);

                if(check == null)
                {
                    employee.PassWord = GetMD5(employee.PassWord);
                    db.Configuration.ValidateOnSaveEnabled = false;
                    db.Employees.Add(employee);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.error = "tài khoan da ton tai";
                    return View(employee);
                }
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string userName, string password)
        {
            if (ModelState.IsValid)
            {


                var f_password = GetMD5(password);
                var data = db.Employees.Where(s => s.UserName.Equals(userName) && s.PassWord.Equals(f_password)).ToList();
                if (data.Count() > 0)
                {
                    //add session
                    Session["FullName"] = data.FirstOrDefault().FullName;
                    Session["UserName"] = data.FirstOrDefault().UserName;
                    Session["ID"] = data.FirstOrDefault().ID;
                    return RedirectToAction("Index");
                    
                }
                else
                {
                    ViewBag.error = "Login failed";
                    return RedirectToAction("Login");
                }
            }
            return View();
        }

        //Logout
        public ActionResult Logout()
        {
            Session.Clear();//remove session
            return RedirectToAction("Login");
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

    }
}
