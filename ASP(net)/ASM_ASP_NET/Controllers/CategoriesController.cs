﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASM_ASP_NET.Context;
using ASM_ASP_NET.Models;

namespace ASM_ASP_NET.Controllers
{
    public class CategoriesController : Controller
    {
        private Restaurant db = new Restaurant();



        public ActionResult Createfood()
        {
            ViewBag.CategoryID = new SelectList(db.categories, "Id", "Category_Name");
            return View();
        }

        // POST: FoodLists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFood([Bind(Include = "Id,NameFood,Price,ListedPrice,CategoryID")] FoodList foodList)
        {
            if (ModelState.IsValid)
            {
                db.FoodLists.Add(foodList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryID = new SelectList(db.categories, "Id", "Category_Name", foodList.CategoryID);
            return View(foodList);
        }
        public ActionResult DeleteFood(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodList foodList = db.FoodLists.Find(id);
            if (foodList == null)
            {
                return HttpNotFound();
            }
            return View(foodList);
        }

        // POST: FoodLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Deletefood(int id)
        {
            FoodList foodList = db.FoodLists.Find(id);
            var CatergryID = foodList.CategoryID;
            db.FoodLists.Remove(foodList);
            db.SaveChanges();
            return RedirectToAction("Details/"+ CatergryID);
        }
        //edit food
        public ActionResult EditFood(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FoodList foodList = db.FoodLists.Find(id);
            if (foodList == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.categories, "Id", "Category_Name", foodList.CategoryID);
            return View(foodList);
        }

        // POST: FoodLists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFood([Bind(Include = "Id,NameFood,Price,ListedPrice,CategoryID")] FoodList foodList)
        {
            var CatergryID = foodList.CategoryID;
            if (ModelState.IsValid)
            {
                db.Entry(foodList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details/"+ CatergryID);
            }
            ViewBag.CategoryID = new SelectList(db.categories, "Id", "Category_Name", foodList.CategoryID);
            return View(foodList);
        }
        // GET: Categories
        public ActionResult Index()
        {
            return View(db.categories.ToList());
        }

        // GET: Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Category_Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(category);
        }

        // GET: Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Category_Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        // GET: Categories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Category category = db.categories.Find(id);
            db.categories.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
