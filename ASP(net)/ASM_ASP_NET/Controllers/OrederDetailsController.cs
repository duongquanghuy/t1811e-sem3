﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASM_ASP_NET.Context;
using ASM_ASP_NET.Models;

namespace ASM_ASP_NET.Controllers
{
    public class OrederDetailsController : Controller
    {
        private Restaurant db = new Restaurant();

        // GET: OrederDetails
        public ActionResult Index()
        {
            var orederDetails = db.orederDetails.Include(o => o.FoodList).Include(o => o.Order);
            return View(orederDetails.ToList());
        }

        // GET: OrederDetails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrederDetail orederDetail = db.orederDetails.Find(id);
            if (orederDetail == null)
            {
                return HttpNotFound();
            }
            return View(orederDetail);
        }

        // GET: OrederDetails/Create
        public ActionResult Create()
        {
            ViewBag.FoodListId = new SelectList(db.FoodLists, "Id", "NameFood");
            ViewBag.OrderID = new SelectList(db.Orders, "Id", "Pyments");
            return View();
        }

        // POST: OrederDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FoodListId,OrderID,Quantity")] OrederDetail orederDetail)
        {
            if (ModelState.IsValid)
            {
                db.orederDetails.Add(orederDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FoodListId = new SelectList(db.FoodLists, "Id", "NameFood", orederDetail.FoodListId);
            ViewBag.OrderID = new SelectList(db.Orders, "Id", "Pyments", orederDetail.OrderID);
            return View(orederDetail);
        }

        // GET: OrederDetails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrederDetail orederDetail = db.orederDetails.Find(id);
            if (orederDetail == null)
            {
                return HttpNotFound();
            }
            ViewBag.FoodListId = new SelectList(db.FoodLists, "Id", "NameFood", orederDetail.FoodListId);
            ViewBag.OrderID = new SelectList(db.Orders, "Id", "Pyments", orederDetail.OrderID);
            return View(orederDetail);
        }

        // POST: OrederDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FoodListId,OrderID,Quantity")] OrederDetail orederDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orederDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FoodListId = new SelectList(db.FoodLists, "Id", "NameFood", orederDetail.FoodListId);
            ViewBag.OrderID = new SelectList(db.Orders, "Id", "Pyments", orederDetail.OrderID);
            return View(orederDetail);
        }

        // GET: OrederDetails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrederDetail orederDetail = db.orederDetails.Find(id);
            if (orederDetail == null)
            {
                return HttpNotFound();
            }
            return View(orederDetail);
        }

        // POST: OrederDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrederDetail orederDetail = db.orederDetails.Find(id);
            db.orederDetails.Remove(orederDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
