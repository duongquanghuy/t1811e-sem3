﻿namespace ASM_ASP_NET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category_Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FoodList",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NameFood = c.String(),
                        Price = c.Int(nullable: false),
                        ListedPrice = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .Index(t => t.CategoryID);
            
            CreateTable(
                "dbo.Customer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        PhoneNumber = c.String(),
                        Address = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Table_Code = c.Byte(nullable: false),
                        DateAndTime = c.DateTime(nullable: false),
                        DateOrder = c.DateTime(nullable: false),
                        ColseTheBill = c.DateTime(nullable: false),
                        Discount = c.Int(nullable: false),
                        Pyments = c.String(),
                        TotalMoney = c.Int(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customer", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.Employee", t => t.EmployeeID, cascadeDelete: true)
                .Index(t => t.EmployeeID)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false),
                        UserName = c.String(nullable: false),
                        PassWord = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrederDetail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FoodListId = c.Int(nullable: false),
                        OrderID = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FoodList", t => t.FoodListId, cascadeDelete: true)
                .ForeignKey("dbo.Order", t => t.OrderID, cascadeDelete: true)
                .Index(t => t.FoodListId)
                .Index(t => t.OrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrederDetail", "OrderID", "dbo.Order");
            DropForeignKey("dbo.OrederDetail", "FoodListId", "dbo.FoodList");
            DropForeignKey("dbo.Order", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Order", "CustomerID", "dbo.Customer");
            DropForeignKey("dbo.FoodList", "CategoryID", "dbo.Category");
            DropIndex("dbo.OrederDetail", new[] { "OrderID" });
            DropIndex("dbo.OrederDetail", new[] { "FoodListId" });
            DropIndex("dbo.Order", new[] { "CustomerID" });
            DropIndex("dbo.Order", new[] { "EmployeeID" });
            DropIndex("dbo.FoodList", new[] { "CategoryID" });
            DropTable("dbo.OrederDetail");
            DropTable("dbo.Employee");
            DropTable("dbo.Order");
            DropTable("dbo.Customer");
            DropTable("dbo.FoodList");
            DropTable("dbo.Category");
        }
    }
}
