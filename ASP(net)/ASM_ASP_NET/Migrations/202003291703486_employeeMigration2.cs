﻿namespace ASM_ASP_NET.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeeMigration2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FoodList", "CategoryID", "dbo.Category");
            DropIndex("dbo.FoodList", new[] { "CategoryID" });
            AlterColumn("dbo.FoodList", "CategoryID", c => c.Int());
            AlterColumn("dbo.Employees", "FullName", c => c.String(nullable: false, maxLength: 30));
            CreateIndex("dbo.FoodList", "CategoryID");
            AddForeignKey("dbo.FoodList", "CategoryID", "dbo.Category", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FoodList", "CategoryID", "dbo.Category");
            DropIndex("dbo.FoodList", new[] { "CategoryID" });
            AlterColumn("dbo.Employees", "FullName", c => c.String(nullable: false));
            AlterColumn("dbo.FoodList", "CategoryID", c => c.Int(nullable: false));
            CreateIndex("dbo.FoodList", "CategoryID");
            AddForeignKey("dbo.FoodList", "CategoryID", "dbo.Category", "Id", cascadeDelete: true);
        }
    }
}
