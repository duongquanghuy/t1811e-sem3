﻿using ASM_ASP_NET.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Context
{
    public class Restaurant:DbContext
    {
        public Restaurant() : base("Restaurant") { }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Category> categories { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<OrederDetail> orederDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<FoodList> FoodLists { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().ToTable("Employees");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);

        }

    }
}