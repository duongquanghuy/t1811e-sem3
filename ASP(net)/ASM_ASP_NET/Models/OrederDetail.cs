﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
  
    public class OrederDetail
    {
        public int Id { get; set; }

        public int FoodListId { get; set; }

        public int OrderID { get; set; }

        public int Quantity { get; set; }

        public virtual FoodList FoodList { get; set; }

        public virtual Order Order { get; set; }
    }
}