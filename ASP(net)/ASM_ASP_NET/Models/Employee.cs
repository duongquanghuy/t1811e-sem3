﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(30 ,MinimumLength =3, ErrorMessage ="cannot be loger tha 30 characters")]
        [Display(Name = "FullName")]
        public string  FullName { get; set; }

        [Required]
        //[Column("UserName")]
        public string UserName { get; set; }

        [Required]
        public string PassWord { get; set; }

        [NotMapped]
        [Required]
        [System.ComponentModel.DataAnnotations.Compare("PassWord")]
        public string ConfirmPassword { get; set; }


        public virtual ICollection<Order> Orders { get; set; }
    }
}