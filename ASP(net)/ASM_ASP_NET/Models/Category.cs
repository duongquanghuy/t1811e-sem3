﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        public string Category_Name { get; set; }

        public virtual ICollection<FoodList> FoodLists { get; set ; }
    }
}