﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    public class Order
    {
        [Key]
        public  int Id { get; set; }

        public byte Table_Code { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}" ,ApplyFormatInEditMode =true) ]
      
        public DateTime DateAndTime { get; set; }

        public DateTime DateOrder { get; set; }

        public DateTime ColseTheBill { get; set; }

        public int Discount { get; set; }
        public string Pyments { get; set; }

        public int TotalMoney { get; set; }

         public int EmployeeID { get; set; }

        public int CustomerID { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual Customer Customer { get; set; }
            
    }
}