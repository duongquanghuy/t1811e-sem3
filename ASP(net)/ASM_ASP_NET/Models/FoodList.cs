﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    public class FoodList
    {
        [Key]
        public int Id { get; set; }
        public string NameFood { get; set; }
        public int Price { get; set; }
        public int ListedPrice { get; set; }

        public int? CategoryID { get; set; }

        public virtual Category Category { get; set; }

    }
}