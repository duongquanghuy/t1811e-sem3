﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        public string FullName {get;set;}

        public string PhoneNumber { get; set; }
        
        public string Address { get; set; }

        public string Email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}