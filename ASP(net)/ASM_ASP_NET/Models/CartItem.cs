﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASM_ASP_NET.Models
{
    [Serializable]
    public class CartItem
    {

        public int? FoodListId { get; set; }
        public string NameFood { get; set; }
        public int Quantity { get; set; }
       
        public int ListedPrice { get; set; }
   
        public int into_money { get { return Quantity * ListedPrice; } }

    }
}