﻿using LambdaExpressons.Context;
using LambdaExpressons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LambdaExpressons.Controllers
{
    public class 
        BookController : Controller
    {
        BookContext db = new BookContext();
        // GET: Book
        public ActionResult Index()
        {
            var bookIndex = db.Books.ToList();
            return View(bookIndex);
        }

        public ActionResult ShowAfter1990()
        {
           
            DateTime date = new DateTime(2000,01,01);
            var book = db.Books.Where(b => b.Published >= date);
         // var book = from b in db.Books where b.Published >= date select b; 
            return View(book.ToList());
            
        }
        public ActionResult ShowAfter2000()
        {
            DateTime date = new DateTime(2000, 01, 01);
            var book = db.Books.Where(b => b.Published <= date);
            return View(book.ToList());
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult Create(Book book)
        {
            try
            {
               
                db.Books.Add(book);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(book);
            }
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Book/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Book/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Book/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
