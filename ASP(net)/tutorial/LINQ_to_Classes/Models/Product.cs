﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace WebApplicationLinqAsp.Models
{
    public class Product
    {
      
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public DateTime EntryDate { get; set; }
    }
}