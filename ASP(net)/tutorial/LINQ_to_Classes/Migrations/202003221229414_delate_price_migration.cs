﻿namespace WebApplicationLinqAsp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delate_price_migration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Product", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Product", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
