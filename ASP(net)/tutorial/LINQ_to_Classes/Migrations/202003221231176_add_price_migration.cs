﻿namespace WebApplicationLinqAsp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_price_migration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Product", "Price");
        }
    }
}
