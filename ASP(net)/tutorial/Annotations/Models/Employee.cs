﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace DataAnnotations.Models
{
    [Bind(Exclude = "AlbumId")]
    public class Employee
    {
        public int Id { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage = "Employee Name is requied")]
        [StringLength(10, MinimumLength = 4)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Employee Address is requied")]
        [StringLength(200)]
        public string Address { get; set; }

        [Required(ErrorMessage = " Salary is requied")]
        [Range(3000,1000000, ErrorMessage ="salary ,must between 3000 anh 1000000")]
        public decimal Salary { get; set; }

        [Required(ErrorMessage ="please enter your email  Address")]
        [DataType(DataType.EmailAddress)]
        [DisplayName ("Email Addresss")]
        [MaxLength(50)]
        public string Email { get; set; }
    }
}