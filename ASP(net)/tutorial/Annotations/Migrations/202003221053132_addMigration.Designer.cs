﻿// <auto-generated />
namespace DataAnnotations.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class addMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "202003221053132_addMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
