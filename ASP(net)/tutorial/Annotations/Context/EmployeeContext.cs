﻿using DataAnnotations.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DataAnnotations.Context
{
    public class EmployeeContext : DbContext
    {
        public EmployeeContext(): base("EmployeeConnection")
        {

        }
        public DbSet<Employee> Employees { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}