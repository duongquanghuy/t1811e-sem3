﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema; // thư viện cần thêm đẻ sự dụng DatabaseGenerated
namespace CodeFirst.Models
{
    public class Blog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)] // Id tự tăng
        public int BlogId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        
    }
}