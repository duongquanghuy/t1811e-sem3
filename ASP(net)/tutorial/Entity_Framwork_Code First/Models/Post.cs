﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirst.Models
{
    public class Post
    {
        public int PostId { get; set; }
        public string Name { get; set; }
        public int? PostDate { get; set; } // int? cho phép nhân giá trị null

        public int PlogID { get; set; }
        public virtual Blog Blog { get; set; }
    }
}