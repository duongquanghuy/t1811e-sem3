﻿using CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Configuration; //khi cập nhật Entity và override database
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CodeFirst.Context
{
    public class BlogContext : DbContext
    {
        public BlogContext() : base("BlogContext")
        {

        }
        public DbSet<Blog> Blogs { get; set; }

        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}